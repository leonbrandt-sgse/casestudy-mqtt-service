import logger from "./logger";
import { EventEmitter } from "events";
import { Probe } from "@leonbrandt/sgse-casestudy-commons";

import * as mqtt from "mqtt";

import { recordService } from "@leonbrandt/sgse-casestudy-commons";

export class Service extends EventEmitter {

	private _mqttClient: mqtt.Client;

	private _livenessPort: number;
	private _readinessPort: number;

	private _livenessProbe: Probe;
	private _readinessProbe: Probe;

	constructor(livenessPort: number, readinessPort: number) {
		super();

		this._livenessPort = livenessPort;
		this._readinessPort = readinessPort;

		this._livenessProbe = new Probe(this._livenessPort);
		this._readinessProbe = new Probe(this._readinessPort);

		this._livenessProbe.on("error", (error: any) => {
			this.emit("probeerror", error);
		});

		this._readinessProbe.on("error", (error: any) => {
			this.emit("probeerror", error);
		});
	}

	public async start(): Promise<void> {
		logger.info("Starting service...");

		logger.debug("Liveness probe: set up");
		this._livenessProbe.setUp();

		try {
			await this.connect(process.env.DB_CONNECTION_URL);
		}
		catch(e) {
			logger.error("Error while connecting to database. Stopping");
			await this.stop();
			return;
		}

		try {
			await this.listen(process.env.MQTT_CONNECTION_URL, process.env.MQTT_TOPIC);
		}
		catch(e) {
			logger.error("Error while connecting to database. Stopping");
			await this.stop();
			return;
		}

		logger.debug("Readyness probe: set up");
		this._readinessProbe.setUp();

		this.emit("ready");
	}

	private async connect(dbConnectionUrl: string): Promise<void> {
		logger.info(`Connecting to database... (${dbConnectionUrl})`);

		await recordService.connect(dbConnectionUrl);

		logger.info("Connected");
	}

	private async listen(mqttConnectionUrl: string, mqttTopic: string): Promise<void> {
		return new Promise<void>((resolve, reject) => {

			logger.info(`Connecting to mqtt-broker... (${mqttConnectionUrl})`);

			this._mqttClient = mqtt.connect(mqttConnectionUrl);

			this._mqttClient.on("error", (error: Error) => {
				throw error;
			});

			this._mqttClient.on("connect", (packet: mqtt.Packet) => {
				logger.info("Connected");
				logger.info(`Subscribing to topic... (${mqttTopic})`);

				this._mqttClient.subscribe(mqttTopic, { qos: 2 }, (error: Error, granted: mqtt.ISubscriptionGrant[]) => {
					if(error) throw error;

					logger.info("Subscribed");

					this._mqttClient.on("message", async (topic: string, payload: Buffer, packet: mqtt.Packet) => {
						logger.debug("> " + payload.toString() + " @ " + topic);

						let parsed;

						try {
							parsed = payload && payload.toString().length > 0 ? JSON.parse(payload.toString()) : undefined;
							if(!parsed.content) throw null;
						}
						catch(e) {
							logger.warn(`Malformed MQTT-Message: ${payload.toString()} @ ${topic}`);
							return;
						}

						await recordService.create(parsed);
					});

					resolve();
				});
			});

		});
	}

	public async stop(): Promise<void> {
		logger.info("Stopping service...");

		logger.debug("Readyness probe: set down");
		this._readinessProbe.setDown();

		// TODO

		logger.debug("Liveness probe: set down");
		this._livenessProbe.setDown();

		this.emit("stopped");
	}

}
