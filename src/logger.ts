import * as winston from "winston";

function create(): winston.Logger {
	return winston.createLogger({
		level: process.env.NODE_ENV === "test" ? process.env.TEST_LOGLEVEL : process.env.LOGLEVEL,
		transports: [
			new winston.transports.Console({
				format: winston.format.simple(),
			}),
		],
	});
}

const logger: winston.Logger = create();
export default logger;
