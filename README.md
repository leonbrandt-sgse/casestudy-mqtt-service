# sgse-casestudy-mqtt-service

## Environment

There are no defaults. Every variable has to be set.

Variable | Description
--- | ---
`LOGLEVEL` | npm-loglevel
`DB_CONNECTION_URL` | URL to MongoDB
`MQTT_CONNECTION_URL` | URL to MQTT-Broker
`MQTT_TOPIC` | MQTT-Topic to listen to
`PROBE_LIVENESS_PORT` | Port to expose for healthcheck (liveness)
`PROBE_READINESS_PORT` | Port to expose for healthcheck (readiness)
